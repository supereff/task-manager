#Task Management System

We would like a new web Task Management System.

* The Task must include the following information:
    * The title of the customer e.g Mr, Mrs, Miss
    * First Name of customer - Mandatory
    * Last Name of customer - Mandatory
    * Scheduled Date - Mandatory
    * Notes - Optional
*	A user must be able to select a specific task in the list and show a pop up/ new page of details for the task they just selected.
*	A user must be able to create new tasks.
*	A user must be able to edit a task.
*	The Tasks must be persisted to a database.
*	There must be a web UI for the user to interact with the system

# Nice to have features:
* The ability to filter Task status’ in the list of tasks
* The ability to sort by First Name, Last Name, and Scheduled Date ascending and descending in the list of tasks
* A responsive website
* A task can have an optional Address linked to it. An address must be contain the following information:
    * Address line 1 - Mandatory
    * Address line 2 - Optional
    * Town - Mandatory
    * County - Mandatory
    * Postcode - Mandatory
* An address must only be associated with one task.
