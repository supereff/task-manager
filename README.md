#Task Management System

The application is built with Java 17, gradle, springboot and angular.

The runs on port 8080. The frontend app can be found on http://localhost:8080/.

API docs can be found at http://localhost:8080/swagger-ui.html#/

## How to run the application
Run the following command on the project directory.
```
./gradlew bootRun
```
