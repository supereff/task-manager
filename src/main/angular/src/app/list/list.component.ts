import { Component, OnInit } from '@angular/core';
import {TaskService} from '../service/task.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  tasks = [];
  userInputs = {
    sort: null,
    status: null
  };

  readonly sortOptions = [
    {label: 'First Name ASC', id: 'firstName,asc'},
    {label: 'First Name DESC', id: 'firstName,desc'},
    {label: 'Last Name ASC', id: 'lastName,asc'},
    {label: 'Last Name DESC', id: 'lastName,desc'},
    {label: 'Scheduled Date ASC', id: 'scheduledDate,asc'},
    {label: 'Scheduled Date DESC', id: 'scheduledDate,desc'}
  ];

  readonly statusOptions =  [
    {label: 'Todo', id: 'TODO'},
    {label: 'In Progress', id: 'IN_PROGRESS'},
    {label: 'Completed', id: 'COMPLETED'}
  ];

  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.search();
  }

  viewAddress (address): string {
    if (!!address && !!address.address1 && !!address.postcode) {
      return address.address1 + ', ' + address.postcode;
    } else {
      return 'N/A';
    }
  }

  search() {
   this.taskService.findAll(this.userInputs).subscribe(
     res => this.tasks = res ,
     res => console.error(res)
     );
  }


}
