import { Component, OnInit } from '@angular/core';
import {TaskService} from '../service/task.service';
import {ActivatedRoute, Router} from '@angular/router';
import { Task } from '../model/task';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id: string = null;
  task: Task = null;

  constructor(private route: ActivatedRoute, private router: Router,  private taskService: TaskService) {
    this.route.params.subscribe(res => this.id = res.id);
  }

  ngOnInit() {
    if (!!this.id) {
      this.taskService.get(this.id).subscribe(
        response => this.task = response,
        response => console.log(response)
      );
    }
  }

  viewAddress(address) {
    if (!!address && !!address.address1 && !!address.postcode) {
      return address.address1 + ', ' + address.postcode;
    } else {
      return 'N/A';
    }
  }

}
