import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { TaskService } from '../service/task.service';
import {Task} from '../model/task';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  id: string = null;
  isNew = true;
  task: Task = {
    id: null,
    title: 'Mr',
    firstName: '',
    lastName: '',
    scheduledDate: '',
    notes: null,
    address: {
      address1: '',
      address2: '',
      town: '',
      county: '',
      postcode: ''
    }
  };

  private readonly addressDefault = {
    address1: '',
    address2: '',
    town: '',
    county: '',
    postcode: ''
  };

  readonly titles = ['Mr', 'Mrs', 'Miss'];

  constructor(private route: ActivatedRoute, private router: Router,  private taskService: TaskService) {
    this.route.params.subscribe(res => {
      this.id = res.id;
      this.isNew = !this.id;
    });
  }

  ngOnInit() {
    if (!this.isNew) {
      this.taskService.get(this.id).subscribe(task =>  {
          if (!task.address) {
            task.address = {
              address1: '',
              address2: '',
              town: '',
              county: '',
              postcode: ''
            }
            Object.assign(task.address, this.addressDefault);
          }
          this.task = task;
        },
        this.alertUser
      );
    }
  }

  save() {
    let add = this.task.address;
    delete this.task["status"];
    if (add == null || !add.address1 ) {
      delete this.task.address;
    }

    if (this.isNew) {
      this.taskService.create(this.task).subscribe(this.sendHome, this.alertUser);
    } else {
      this.taskService.update(this.task).subscribe(this.sendHome, this.alertUser);
    }
  }

  inProgressTask () {
    this.taskService.inProgressTask(this.task.id).subscribe(this.sendHome, this.alertUser);
  }

  completeTask () {
    this.taskService.completeTask(this.task.id).subscribe(this.sendHome, this.alertUser);
  }

  sendHome() {
    // this.router.navigate(['']);
    location.href = '';
  }

  alertUser(response) {
    console.error(response);
    let list = response.errors.map(obj => obj.defaultMessage);
    alert(list);
  }

}
