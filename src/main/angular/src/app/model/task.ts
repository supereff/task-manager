export interface Task {
  id?: string;
  title?: string;
  firstName: string;
  lastName: string;
  scheduledDate: string;
  notes?: string;
  status?: string;
  address?: {
    address1: string;
    address2?: string;
    town: string;
    county: string;
    postcode: string;
  };
}
