import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Task} from '../model/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  readonly baseUrl = 'http://localhost:8080/api/tasks';

  constructor(private _http: HttpClient) { }

  get(id): Observable<Task> {
    const url = this.baseUrl + '/' + id;
    return this._http.get<Task>(url);
  }

  findAll (userInputs): Observable<Task[]> {
    let url = this.baseUrl + '?a=a';
    if (!!userInputs.sort) {
      url += '&sort=' + userInputs.sort;
    }
    if (!!userInputs.status) {
      url += '&filterByStatus=' + userInputs.status;
    }
    return this._http.get<Task[]>(url);
  }

  create (task: Task): Observable<any> {
    return this._http.post(this.baseUrl, task);
  }

  update (task: Task): Observable<any> {
    return this._http.put(this.baseUrl, task);
  }

  inProgressTask (taskId): Observable<any> {
    const url = this.baseUrl + '/' + taskId + '/in-progress';
    return this._http.get(url);
  }

  completeTask (taskId): Observable<any> {
    const url = this.baseUrl + '/' + taskId + '/completed';
    return this._http.get(url);
  }
}
