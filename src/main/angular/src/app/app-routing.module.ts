import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent} from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { ViewComponent} from './view/view.component';

const routes: Routes = [
  { path: '', component: ListComponent},
  { path: 'tasks/new', component: EditComponent},
  { path: 'tasks/view/:id', component: ViewComponent},
  { path: 'tasks/edit/:id', component: EditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
