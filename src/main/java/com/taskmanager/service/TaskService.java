package com.taskmanager.service;

import com.taskmanager.exception.NotFoundException;
import com.taskmanager.model.Task;
import com.taskmanager.model.Task.Status;
import org.springframework.data.domain.Sort;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

public interface TaskService {

	/**
	 * Creates a task.
	 * We assume that the task is valid.
	 *
	 * @param task task to be saved
	 * @return new id of task
	 */
	@Nonnull
	Long createTask(@Nonnull Task task);

	void updateTask(@Nonnull Task task);

	/**
	 * Get an Valid task.
	 *
	 * @param taskId id of specified task
	 * @return returns specified task of throws an exception
	 * @throws NotFoundException if the task does not exist
	 */
	@Nonnull
	Task getOrThrow(@Nonnull Long taskId) throws NotFoundException;

	/**
	 * Get an task if it exists.
	 *
	 * @param taskId id of specified task
	 */
	@Nonnull
	Optional<Task> getTask(@Nonnull Long taskId);

	/**
	 * Sets the task to In Progress
	 *
	 * @param taskId id of the task
	 * @throws NotFoundException is task does not exist
	 * @throws IllegalStateException is the task is completed or already in-progress
	 */
	void taskInProgress(@Nonnull Long taskId) throws NotFoundException, IllegalStateException;

	/**
	 * Sets the task to completed.
	 *
	 * @param taskId id of the task
	 * @throws NotFoundException is task does not exist
	 * @throws IllegalStateException is the task is already done
	 */
	void taskCompleted(@Nonnull Long taskId) throws NotFoundException, IllegalStateException;

	@Nonnull
	List<Task> findBy(@Nullable Status filterByStatus, @Nonnull Sort sort);
}
