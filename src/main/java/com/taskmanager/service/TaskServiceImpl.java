package com.taskmanager.service;

import com.taskmanager.exception.NotFoundException;
import com.taskmanager.model.Address;
import com.taskmanager.model.Task;
import com.taskmanager.model.Task.Status;
import com.taskmanager.repository.TaskRepository;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import jakarta.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.taskmanager.model.Task.Status.COMPLETED;
import static com.taskmanager.model.Task.Status.IN_PROGRESS;
import static com.taskmanager.model.Task.Status.TODO;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskRepository taskRepository;

	@PostConstruct
	void createDefaultDummyTask() {
		Task task1 = new Task();
		task1.setTitle(Task.Title.Mr);
		task1.setFirstName("Eff");
		task1.setLastName("Miah");
		task1.setScheduledDate(LocalDate.now());
		task1.setStatus(TODO);
		task1.setNotes("Something...");
		taskRepository.save(task1);

		Task task2 = new Task();
		task2.setTitle(Task.Title.Mr);
		task2.setFirstName("Joseph");
		task2.setLastName("Mcelroy");
		task2.setScheduledDate(LocalDate.now().plusDays(1));
		task2.setStatus(TODO);
		Address address = new Address();
		address.setAddress1("9 Random Address");
		address.setTown("London");
		address.setCounty("Greater London");
		address.setPostcode("E1 1AA");
		task2.setAddress(address);
		taskRepository.save(task2);

	}

	@Nonnull
	@Override
	public Long createTask(@Nonnull Task task) {
		Preconditions.checkArgument(task.getId() == null, "id cannot be set");
		task.setStatus(TODO);
		Task result = taskRepository.save(task);
		return result.getId();
	}

	@Override
	public void updateTask(@Nonnull Task input) {
		Task task = getOrThrow(input.getId());

		if (input.getStatus() == null) {
			input.setStatus(task.getStatus());
		}

		taskRepository.save(input);
	}

	@Override
	public void taskInProgress(@Nonnull Long taskId) {
		Task task = getOrThrow(taskId);
		Preconditions.checkState(task.getStatus() != IN_PROGRESS, "task is already in progress");
		Preconditions.checkState(task.getStatus() != COMPLETED, "sorry, you cannot put completed task to in progress");

		task.setStatus(IN_PROGRESS);

		updateTask(task);
	}

	@Override
	public void taskCompleted(@Nonnull Long taskId) {
		Task task = getOrThrow(taskId);
		Preconditions.checkState(task.getStatus() != COMPLETED, "task is already completed");

		task.setStatus(COMPLETED);

		updateTask(task);
	}

	@Nonnull
	@Override
	public Task getOrThrow(@Nonnull Long id) throws NotFoundException {
		return getTask(id).orElseThrow(() ->  new NotFoundException("Task does not exist"));
	}

	@Nonnull
	@Override
	public Optional<Task> getTask(@Nonnull Long id) {
		return taskRepository.findById(id);
	}

	@Nonnull
	@Override
	public List<Task> findBy(@Nullable Status filterByStatus, @Nonnull Sort sort) {
		Task filterBy = new Task();
		if (filterByStatus != null) {
			filterBy.setStatus(filterByStatus);
		}
		Example<Task> example = Example.of(filterBy);
		return taskRepository.findAll(example, sort);
	}

}
