package com.taskmanager.controller;

import com.taskmanager.model.Task;
import com.taskmanager.model.Task.Status;
import com.taskmanager.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;
import jakarta.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/tasks", produces = APPLICATION_JSON_VALUE)
public class TaskController {

	@Autowired
	private TaskService taskService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> createTask(@Valid @RequestBody Task task) {
		Long id = taskService.createTask(task);
		return ResponseEntity.created(URI.create("/api/tasks/"+id)).build();
	}

	@PutMapping
	public ResponseEntity<?> updateTask(@Valid @RequestBody Task task) {
		taskService.updateTask(task);
		return ResponseEntity.ok().build();
	}

	@GetMapping("{taskId}")
	public Task getTask(@PathVariable Long taskId) {
		return taskService.getOrThrow(taskId);
	}

	@GetMapping("{taskId}/in-progress")
	public ResponseEntity<?> taskInProgress(@PathVariable Long taskId) {
		taskService.taskInProgress(taskId);
		return ResponseEntity.ok().build();
	}

	@GetMapping("{taskId}/completed")
	public ResponseEntity<?> taskCompleted(@PathVariable Long taskId) {
		taskService.taskCompleted(taskId);
		return ResponseEntity.ok().build();
	}

	@GetMapping
	public List<Task> findAll(@RequestParam(value = "filterByStatus", required = false)Status filterByStatus, @Nonnull Sort sort) {
		return taskService.findBy(filterByStatus, sort);
	}
}
