package com.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;

@Entity
@Table(name = "address")
@Data
public class Address {

	@JsonIgnore
	@Id
	@GeneratedValue
	@Column( name = "id", nullable = false)
	private Integer id;

	@NotBlank(message = "address line must be specified")
	@Column(nullable = false)
	private String address1;

	@Column
	private String address2;

	@NotBlank(message = "town must be specified")
	@Column(nullable = false)
	private String town;

	@NotBlank(message = "county must be specified")
	@Column(nullable = false)
	private String county;

	@NotBlank(message = "postcode must be specified")
	@Column(nullable = false)
	private String postcode;


}
