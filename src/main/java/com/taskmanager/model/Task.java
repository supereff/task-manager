package com.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY;

@Entity
@Table(name = "task")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Task {

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private Title title;

	@Column(name="first_name", nullable = false)
	@NotBlank(message = "first name must be specified")
	private String firstName;

	@Column(name="last_name", nullable = false)
	@NotBlank(message = "last name must be specified")
	private String lastName;

	@Column(name="scheduled_date", nullable = false)
	@NotNull(message = "scheduled Date must be specified")
	private LocalDate scheduledDate;

	//This value cannot be directly modified by the user
	@JsonProperty(access = READ_ONLY)
	@Column(nullable = false)
	private Status status;

	@Column
	private String notes;

	@Valid
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "address_id", referencedColumnName = "id")
	private Address address;

	public enum Title {
		Mr, Mrs, Miss
	}

	public enum Status {
		TODO, IN_PROGRESS, COMPLETED
	}
}
