package com.taskmanager.service;

import com.taskmanager.exception.NotFoundException;
import com.taskmanager.model.Task;
import com.taskmanager.repository.TaskRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static com.taskmanager.model.Task.Status.COMPLETED;
import static com.taskmanager.model.Task.Status.IN_PROGRESS;
import static com.taskmanager.model.Task.Status.TODO;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TaskServiceImplTest {

	private static final Long EXISTING_TASK_ID = 2L;
	private static final Long NON_EXISTING_TASK_ID = -1L;

	@InjectMocks
	private TaskServiceImpl underTest;

	@Mock
	private TaskRepository taskRepository;

	@Mock
	private Task mockSavedTask;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);


		when(taskRepository.findById(eq(EXISTING_TASK_ID))).thenReturn(Optional.of(mockSavedTask));
		when(taskRepository.findById(eq(NON_EXISTING_TASK_ID))).thenReturn(Optional.empty());

		when(mockSavedTask.getId()).thenReturn(EXISTING_TASK_ID);

		underTest.createDefaultDummyTask();
	}

	@Test
	public void test_createTask_success() {
		Task task = new Task();
		when(taskRepository.save(task)).thenReturn(mockSavedTask);

		Long result = underTest.createTask(task);

		assertNotNull(result);
		assertEquals(EXISTING_TASK_ID, result);
		verify(taskRepository).save(task);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_createTask_cannotHaveId () {
		Task task = new Task();
		task.setId(1L);

		underTest.createTask(task);
	}

	@Test
	public void test_updateTask_success() {
		Task task = new Task();
		task.setId(EXISTING_TASK_ID);

		underTest.updateTask(task);

		verify(taskRepository).save(task);
	}

	@Test
	public void test_updateTask_copyStatusOver() {
		Task task = new Task();
		task.setId(EXISTING_TASK_ID);

		when(mockSavedTask.getStatus()).thenReturn(Task.Status.IN_PROGRESS);

		underTest.updateTask(task);

		verify(taskRepository).save(task);
		verify(mockSavedTask).getStatus();
	}


	@Test
	public void test_taskInProgress_TaskStatusIsTODO() {

		when(mockSavedTask.getStatus()).thenReturn(TODO);

		underTest.taskInProgress(EXISTING_TASK_ID);

		verify(mockSavedTask).setStatus(IN_PROGRESS);
		verify(taskRepository).save(mockSavedTask);
	}

	@Test(expected = IllegalStateException.class)
	public void test_taskInProgress_TaskStatusIsInProgress() {

		when(mockSavedTask.getStatus()).thenReturn(IN_PROGRESS);

		underTest.taskInProgress(EXISTING_TASK_ID);

	}

	@Test(expected = IllegalStateException.class)
	public void test_taskInProgress_TaskStatusIsCompleted() {

		when(mockSavedTask.getStatus()).thenReturn(COMPLETED);

		underTest.taskInProgress(EXISTING_TASK_ID);
	}

	@Test
	public void test_taskCompleted_TaskStatusIsTODO() {

		when(mockSavedTask.getStatus()).thenReturn(TODO);

		underTest.taskCompleted(EXISTING_TASK_ID);

		verify(mockSavedTask).setStatus(COMPLETED);
		verify(taskRepository).save(mockSavedTask);
	}

	@Test
	public void test_taskCompleted_TaskStatusIsInProgress() {

		when(mockSavedTask.getStatus()).thenReturn(IN_PROGRESS);

		underTest.taskCompleted(EXISTING_TASK_ID);

		verify(mockSavedTask).setStatus(COMPLETED);
		verify(taskRepository).save(mockSavedTask);

	}

	@Test(expected = IllegalStateException.class)
	public void test_taskCompleted_TaskStatusIsCompleted() {

		when(mockSavedTask.getStatus()).thenReturn(COMPLETED);

		underTest.taskCompleted(EXISTING_TASK_ID);
	}

	@Test
	public void test_getOrThrow_exists() {
		Task result = underTest.getOrThrow(EXISTING_TASK_ID);

		assertNotNull(result);
		assertEquals(EXISTING_TASK_ID, result.getId());
	}

	@Test(expected = NotFoundException.class)
	public void test_getOrThrow_DoesNotExist() {

		Task result = underTest.getOrThrow(NON_EXISTING_TASK_ID);

		assertNull(result);
	}

	@Test
	public void test_getTask_exists() {
		Optional<Task> task = underTest.getTask(EXISTING_TASK_ID);

		assertNotNull(task);
		assertTrue(task.isPresent());
		assertEquals(EXISTING_TASK_ID, task.get().getId());
	}

	@Test
	public void test_getTask_DoesNotExist() {
		Optional<Task> task = underTest.getTask(NON_EXISTING_TASK_ID);

		assertNotNull(task);
		assertFalse(task.isPresent());
	}

}