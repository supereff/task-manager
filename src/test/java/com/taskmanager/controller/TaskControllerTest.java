package com.taskmanager.controller;

import com.taskmanager.Application;
import com.taskmanager.model.Address;
import com.taskmanager.repository.TaskRepository;
import com.taskmanager.model.Task;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskControllerTest {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private TestRestTemplate restTemplate;


	@Before
	public void setup() {
		taskRepository.deleteAll();
	}

	@Test
	public void test_createTask(){
		Address address = of("A House", null, "London", "Greater London", "E1 1AA");
		Task task = of(Task.Title.Mr, "TestName", "TestSurname", LocalDate.now(), "something", address);

		ResponseEntity<Void> response = saveTask(task);
		Assert.assertEquals("sucessful create", HttpStatus.CREATED, response.getStatusCode());

		address.setAddress1(" ");
		response = saveTask(task);
		Assert.assertEquals("missing mandatory address1", HttpStatus.BAD_REQUEST, response.getStatusCode());

		address.setAddress1("A House");
		address.setAddress2("");
		response = saveTask(task);
		Assert.assertEquals("missing optional address2",HttpStatus.CREATED, response.getStatusCode());

		address.setTown("");
		response = saveTask(task);
		Assert.assertEquals("missing mandatory town",HttpStatus.BAD_REQUEST, response.getStatusCode());

		address.setTown("London");
		address.setCounty("");
		response = saveTask(task);
		Assert.assertEquals("missing mandatory county", HttpStatus.BAD_REQUEST, response.getStatusCode());

		address.setTown("Greater London");
		address.setPostcode("");
		response = saveTask(task);
		Assert.assertEquals("missing mandatory postcode", HttpStatus.BAD_REQUEST, response.getStatusCode());

		task.setAddress(null);
		response = saveTask(task);
		Assert.assertEquals("missing optional address objec", HttpStatus.CREATED, response.getStatusCode());

		task.setNotes(null);
		response = saveTask(task);
		Assert.assertEquals("missing optional notes", HttpStatus.CREATED, response.getStatusCode());

		task.setTitle(null);
		response = saveTask(task);
		Assert.assertEquals("missing optional title",HttpStatus.CREATED, response.getStatusCode());

		task.setFirstName("");
		response = saveTask(task);
		Assert.assertEquals("missing mandatory first name", HttpStatus.BAD_REQUEST, response.getStatusCode());

		task.setFirstName("TestName");
		task.setLastName("");
		response = saveTask(task);
		Assert.assertEquals("missing mandatory lastname", HttpStatus.BAD_REQUEST, response.getStatusCode());

		task.setLastName("TestSurname");
		task.setScheduledDate(null);
		response = saveTask(task);
		Assert.assertEquals("missing mandatory schedule date", HttpStatus.BAD_REQUEST, response.getStatusCode());

	}

	@Test
	public void update_task() {
		Task task = createTask();
		ResponseEntity<Void> createResponse = saveTask(task);

		Task createdTask = getTask(createResponse).getBody();
		Assert.assertNotNull(createdTask);

		String updatedFirstName = "New First Name";
		String updatedAddress1 = "New Address 1";
		createdTask.setFirstName(updatedFirstName);
		createdTask.getAddress().setAddress1(updatedAddress1);

		updateTask(createdTask);
		Task updatedTask = getTask(createdTask.getId()).getBody();
		Assert.assertNotNull(updatedTask);
		Assert.assertEquals("first name has been updated", updatedFirstName, updatedTask.getFirstName());
		Assert.assertEquals("address1 has been updated", updatedAddress1, updatedTask.getAddress().getAddress1());
		Assert.assertEquals("last name has not been updated", task.getLastName(), updatedTask.getLastName());
	}



	private Task createTask(){
		Address address = of("A House", null, "London", "Greater London", "E1 1AA");
		Task task = of(Task.Title.Mr, "TestName", "TestSurname", LocalDate.now(), "something", address);
		return task;
	}

	private Task of(@Nullable Task.Title title, @Nonnull String firstName, @Nonnull String lastName, @Nonnull LocalDate scheduledDate, String notes, Address address) {
		Task task = new Task();
		task.setTitle(title);
		task.setFirstName(firstName);
		task.setLastName(lastName);
		task.setScheduledDate(scheduledDate);
		task.setNotes(notes);
		task.setAddress(address);
		return task;
	}

	private static Address of(String address1, String address2, String town, String county, String postcode) {
		Address address = new Address();
		address.setAddress1(address1);
		address.setAddress2(address2);
		address.setCounty(county);
		address.setTown(town);
		address.setPostcode(postcode);
		return address;
	}

	private ResponseEntity<Void> saveTask(Task task) {
		return restTemplate.postForEntity("/api/tasks", task, Void.class);
	}

	private void updateTask(Task task) {
		restTemplate.put("/api/tasks", task);
	}

	private ResponseEntity<Task> getTask(Long taskId) {
		return restTemplate.getForEntity("/api/tasks/"+taskId, Task.class);
	}

	private void inProgress(Long taskId) {
		restTemplate.put("/api/tasks/"+taskId+"/in-progress", Task.class);
	}

	private void completeTask(Long taskId) {
		restTemplate.put("/api/tasks/"+taskId+"/completed", Task.class);
	}

	private ResponseEntity<Task> getTask(ResponseEntity<Void> responseEntity) {
		String getEntityLocation = responseEntity.getHeaders().get("Location").get(0);
		return restTemplate.getForEntity(getEntityLocation, Task.class);
	}

}